function loadPage() {
    fetch(`./graphql?query={
        locomotives:getLocomotives{
            id
            number
            name
            owner
        }
        cars:getCars{
            id
            number
            name
            owner
        }
        }`, { "method": "GET" }).then(response => { return response.json() }).then(data => {
        var cars = data["data"]["cars"]
        var locomotives = data["data"]["locomotives"]
        $.each(cars, function (index, car) {
            $("#cars").append(
                `<div class="card">
                            <div class="card-body">
                                <p class="card-text>${cars.number}</p>
                                <p class="card-text>${cars.type}</p>
                                <p class="card-text>${cars.owner}</p>
                            </div>
                            <div> class="card-footer"
                        </div>`
            )
        })
        $.each(locomotives, function (index, locomotive) {
            $("#locomotives").append(`<div class="card">
                        <div class="card-body">
                            <p class="card-text>${locomotive.number}</p>
                            <p class="card-text>${locomotive.type}</p>
                            <p class="card-text>${locomotive.owner}</p>
                        </div>
                        <div> class="card-footer"
                            <a v-on:click= class="btn btn-primary"></a>
                        </div>        
                    </div>
                        `)
        })
    })
}
function deleteCars(){
    fetch(`/graphql?query=mutation{
        deleteLocomotive(id:cars.id){
            id

        }
    }`).then(response=>{return response.json()}).then(data=>{

    })
}