var mongoose = require('mongoose');
var schema=mongoose.Schema

var scheduleEntry=new schema({
    direction:String,
    class:String,
    name:String,
    stops:[String],
    departureTIme:String,
    arrivalTime:String,
    startpoint:String,
    endpoint:String
})

exports.scheduleEntry=mongoose.model("scheduleEntry",scheduleEntry)