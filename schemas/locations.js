var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var siding=new Schema({
    name:String,
    length:Number,
    milepost:Number,
    cars:[mongoose.Schema.Types.ObjectId]
})


var Location=new Schema({
    name: String,
    milepost:Number,
    mapX:Number,
    mapY:Number,
    sidings:[siding],
})
var line=new Schema({
    name:String,
    locations:[Location]
})
exports.Location=mongoose.model("Location",Location)
exports.Sidings=mongoose.model("Sidings",siding)
exports.line=mongoose.model("line",line)