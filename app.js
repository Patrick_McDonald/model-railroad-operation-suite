var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var express_graphql = require('express-graphql');
var { buildSchema } = require('graphql');
var MongoClient = require('mongodb').MongoClient;
var mongoose = require('mongoose');
var Schema = mongoose.Schema
var MapLocations = require("./schemas/locations")
var roster=require("./schemas/roste")
var schedule=require("./schemas/schedule")
mongoose.connect('mongodb://localhost:27017/operationSuite')
var db = mongoose.connection;
db.on('error', ()=> {console.log( '---FAILED to connect to mongoose')})
db.once('open', () => {
 console.log( '+++Connected to mongoose')
})
var Schema = buildSchema(`
type Location{
    id:String
    name:String
    sidings:[siding]
    milepost:Int
}
type siding{
    id:String
    name:String
    length:Int
    milepost:Int
}
type line{
    id:String
    name:String
    locations:[Location]
}
type car{
    id:String,
    number:Int,
    type:String,
    owner:String,
}

type locomotive{
    id:String,
    number:Int,
    type:String,
    owner:String
}
type train{
    direction:String,
    class:String,
    name:String,
    stops:String,
    departureTIme:String,
    arrivalTime:String,
    startpoint:String,
    endpoint:String
}

type Query{
    getLines:[line]
    getLocations:[Location]
    getLocation(lineid:String!,id:String!):Location
    getSidings(lineid:String!,LOcationid:String!):[siding]
    getSiding(lineid:String!,LOcationid:String!,sidingID:Int!):siding
    getCars:[car]
    getLocomotive:[locomotive]
    getTrains(direction:String!):[train]

}

type Mutation{
    createLine(name:String!):line
    createLocation(lineid:String!,name: String!,milepost: Int!):Location
    createSiding(lineid:String!,locationId:String!,name: String!,milepost: Int!,length:Int):siding
    updateLOcationPosition(id:Int!):Location
    deleteLocation(id:String!):String
    createCar(number:Int!,type:String!,owner:String!):locomotive
    createLocomotive(number:Int!,type:String!,owner:String!):car
    createTrain(name:String!):train
    deleteTrain(id:String!):train
}
`)

/// catch 404 and forwarding to error handler

var root = {
    getLines:async()=>{
        return await MapLocations.line.find();
    },
    getLocations: async () => {
         var data=await MapLocations.Location.find();
         console.log(data)
         return data;

    },
    getLocation: async ({id}) => {
        return await MapLocations.Location.findOne(MongoClient.ObjectId(id));
    },
    getSidings:async({lineid,LOcationid})=>{
        var line=await MapLocations.line.findOne({id:lineid})
        //console.log(line)
        return await line.locations.id({id:LOcationid}).sidings
    },
    getSiding:async({LOcationid,sidingID})=>{
        return await MapLocations.Location.findOne(LOcationid).sidings.id(objectId(sidingID))
    },
    getCars:async()=>{
        //console.log(roster.cars)
        return await roster.cars.find()
    },
    getLocomotive:async()=>{
        return await roster.locomotives.find()
    },

    createLine:async({name})=>{
        return await MapLocations.line.create({name:name,
                                                locations:[]})
    },
    getTrains:async({direction})=>{
        return await schedule.scheduleEntry.findOne({direction:direction})
    },
    createTrain:async()=>{

    },
    createLocation:async({id,name,milepost})=>{
        console.log({name,milepost})
        var line=await MapLocations.line.findOne({id:id})
        console.log(line)

        var newLocation=await MapLocations.Location({
            name: name,
            milepost:milepost,
            sidings:[],
        })
        line.locations.push(newLocation)
        return await line.save()
    },
    createSiding:async({lineId,locationId,name,milepost,length})=>{

        var newSiding= await MapLocations.Sidings.create({
            name:name,
            milepost:milepost,
            length:length,
        })
       var line= await MapLocations.line.findOne({id:lineId})
       console.log("line",line.locations.id(locationId))

        console.log("found siding")
        await line.locations.id(locationId).sidings.push(newSiding);
        await line.save()
        return newSiding 

    },
    deleteLocation:async({id,locationId})=>{
        return await MapLocations.line.findOne(MongoClient.ObjectId(lineId)).findOneAndRemove(objectId(locationId))
    },
    createCar:async({number,Cartype,owner})=>{
        console.log(number,Cartype,owner)
        return await roster.cars.create({
            number:number,
            type:Cartype,
            owner:owner
        })
    },
    createLocomotive:async({number,Cartype,owner})=>{
        return await roster.locomotive.create({
            number:number,
            type:Cartype,
            owner:owner
        })
    },
    createTrain:async({})=>{
        return await schedule.scheduleEntry.create({})
    },
    updateTrain:async({})=>{
        return await schedule.scheduleEntry.create({})
    },
    deleteLocomotive:async({id})=>{
        return await roster.locomotive.findOneAndRemove({id:id})
    },
    deleteLocomotive:async({id})=>{
        return await roster.locomotive.findOneAndRemove({id:id})
    },
    deleteTrain:async({id})=>{
        return schedule.scheduleEntry.findOneAndRemove({id:id})
    }

}


/// error handlers

// development error handler
// will print stacktrace
// Create an express server and a GraphQL endpoint
var app = express();
app.use('/graphql', express_graphql({
    schema: Schema,
    rootValue: root,
    graphiql: true
}));
app.use(favicon());
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
module.exports = app;
